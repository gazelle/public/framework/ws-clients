# Gazelle web-service clients

This project is a library of Java 7 clients of various Gazelle APIs.

It includes in the module `gazelle-ws-clients`:

* DCC Validation Service Client
* Demographic Data Server Client
* HL7v2 Validation Client
* Message WS Client (Notifications in Test Managements)
* Object Validator Client (deprecated Schematron client)
* Simulator Manager Client
* ITI71 Validator Client (CH-IUA Validator client)
* Model Based Validator Client
* NIST HL7v2 Validation Client
* Proxy Web-Service Client
* Schematron Validator WS client
* Schematron WS Client (deprecated)
* XDW Validator client

It includes in the module `gazelle-token-ws-client`:

* Token Service Client ([see documentation](gazelle-token-ws-client/README.md))

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```


