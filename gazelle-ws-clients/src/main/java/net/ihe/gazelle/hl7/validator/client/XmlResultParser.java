package net.ihe.gazelle.hl7.validator.client;

import com.sun.org.apache.xerces.internal.impl.dv.DVFactoryException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XmlResultParser extends DefaultHandler {

   private static Logger log = Logger.getLogger("net.ihe.gazelle.hl7.validator.client.XmlResultParser");

   protected byte[] result;
   protected String validationStatus;
   protected String currentText;
   protected String errorCountAsString;
   protected String warningCountAsString;

   /**
    * Sets the result and parse the document
    */
   public XmlResultParser(String result) {
      if (result != null) {
         this.result = result.getBytes();
         parse();
      } else {
         this.result = null;
      }
   }

   /**
    * Returns the validation status extracted from the XML string
    *
    * @return null if the "ValidationTestResult" element is not present in the XML string
    */
   public String getValidationStatus() {
      return this.validationStatus;
   }

   /**
    * returns the number of errors
    *
    * @return -1 if the "NrOfValidationErrors" element is not present in the XML string
    */
   public int getErrorCount() {
      if (errorCountAsString == null || errorCountAsString.isEmpty()) {
         return -1;
      } else {
         return Integer.parseInt(errorCountAsString);
      }
   }

   /**
    * returns the number of warnings
    *
    * @return -1 if the "NrOfValidationWarnings" element is not present in the XML String
    */
   public int getWarningCount() {
      if (warningCountAsString == null || warningCountAsString.isEmpty()) {
         return -1;
      } else {
         return Integer.parseInt(warningCountAsString);
      }
   }

   protected void parse() {
      try {
         SAXParserFactory saxFactory;
         SAXParser saxParser;
         try {
            saxFactory = SAXParserFactory
                  .newInstance("com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl", ClassLoader.getSystemClassLoader());
            saxParser = saxFactory.newSAXParser();
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
         } catch (Throwable t) {
            log.log(Level.WARNING,
                  "Unable to instantiate com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl, fall back on default SAXParser. DTD and " +
                        "SCHEMA injection safeguard is disabled.");
            saxFactory = SAXParserFactory.newInstance();
            saxParser = saxFactory.newSAXParser();
         }
         ByteArrayInputStream bais = new ByteArrayInputStream(result);
         Reader reader = new InputStreamReader(bais);
         InputSource source = new InputSource(reader);
         source.setEncoding("UTF-8");
         // parse file
         saxParser.parse(source, this);
      } catch (SAXException e) {
         log.log(Level.SEVERE, e.getMessage());
      } catch (IOException e) {
         log.log(Level.SEVERE, e.getMessage());
      } catch (ParserConfigurationException e) {
         log.log(Level.SEVERE, e.getMessage());
      }
   }

   public void endElement(String uri, String localName, String qName) throws SAXException {
      if (qName.equals("ValidationTestResult")) {
         validationStatus = currentText;
      } else if (qName.equals("NrOfValidationErrors")) {
         errorCountAsString = currentText;
      } else if (qName.equals("NrOfValidationWarnings")) {
         warningCountAsString = currentText;
      }
   }

   public void characters(char ch[], int start, int length) throws SAXException {
      currentText = new String(ch, start, length);
   }

}
