package net.ihe.gazelle.mb.validator.client;

public class ServiceOperationError extends Exception {

   public ServiceOperationError() {
   }

   public ServiceOperationError(String s) {
      super(s);
   }

   public ServiceOperationError(String s, Throwable throwable) {
      super(s, throwable);
   }

   public ServiceOperationError(Throwable throwable) {
      super(throwable);
   }
}
