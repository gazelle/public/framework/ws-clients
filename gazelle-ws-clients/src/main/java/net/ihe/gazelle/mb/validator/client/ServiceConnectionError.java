package net.ihe.gazelle.mb.validator.client;

public class ServiceConnectionError extends Exception {

   public ServiceConnectionError() {
   }

   public ServiceConnectionError(String s) {
      super(s);
   }

   public ServiceConnectionError(String s, Throwable throwable) {
      super(s, throwable);
   }

   public ServiceConnectionError(Throwable throwable) {
      super(throwable);
   }
}
