package net.ihe.gazelle.dcc.validator.client;

import net.ihe.gazelle.validationreport.ValidationReportDTO;
import net.ihe.gazelle.validator.dcc.*;
import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import java.rmi.RemoteException;
import java.util.*;

public class DCCValidator {

    private static final long DEFAULT_TIMEOUT = 60000;


    private static Logger log = LoggerFactory.getLogger(DCCValidator.class);

    /**
     * Target endpoint to contact the model based validator (must extends AbstractModelBasedValidator
     */
    protected String endpoint;

    /**
     * Detailed validation result returned by the simulator
     */
    protected ValidationReportDTO validationResult;

    /**
     * Decoded QRCode
     */
    protected String decodedMessage;

    /**
     * Stub to contact validator
     */
    protected DccValidationServiceStub stub;
    /**
     * Timeout (in millisecond) on client side when waiting for server response
     */
    protected long timeout;

    /**
     * Constructor
     *
     * @param endpoint : string endpoint
     */
    public DCCValidator(String endpoint) {
        this.endpoint = endpoint;
        this.stub = null;
        this.timeout = DEFAULT_TIMEOUT;
    }

    public DCCValidator(String endpoint, long timeout) {
        this.endpoint = endpoint;
        this.stub = null;
        this.timeout = timeout;
    }

    /**
     * Returns the list of validators available on the contacted service side
     *
     * @return List of validators available
     */
    public Map<String,String> listAvailableValidators() {
        Map<String,String> availableValidators = new LinkedHashMap<>();
        try {
            getStub();
            GetValidators getValidators = new GetValidators();
            GetValidatorsE getValidatorsE = new GetValidatorsE();
            getValidatorsE.setGetValidators(getValidators);

            Validator[] validators = stub.getValidators(getValidatorsE).getGetValidatorsResponse().getValidators().getValidator();
            if (validators != null && validators.length > 0) {
                for (int i = 0; i < validators.length; i++) {
                    availableValidators.put(validators[i].getKeyword(),validators[i].getName());
                }
            }
        } catch (AxisFault e) {
            log.error("Unable to connect to the validation service at " + this.endpoint);
        } catch (RemoteException e) {
            log.error("Cannot retrieve the list of validators: " + e.getMessage());
        }
        return availableValidators;
    }

    private void getStub() throws AxisFault {
        if (stub == null) {
            stub = new DccValidationServiceStub(this.endpoint);
            stub._getServiceClient().getOptions().setProperty(org.apache.axis2.Constants.Configuration.DISABLE_SOAP_ACTION, true);
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, (int) timeout);
            stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, (int) timeout);
        }
    }

    /**
     * Validate the document against the given validator
     *
     * @param document:  the document to validate (plain text or base64 encoded)
     * @param validator: the validator to use
     * @return the detailed result as forwarded by the validator
     */
    public ValidationReportDTO validate(DataHandler document, String validator) throws UnkownValidatorExceptionException {
        this.validationResult = null;

        if (document == null) {
            log.error("The document to validate is either null or empty");
        } else if (validator == null || validator.isEmpty()) {
            log.error("The validator to call is either null or empty");
        } else {
            try {
                getStub();
                Validate validateWithReport = new Validate();
                validateWithReport.setObject(document);
                validateWithReport.setValidator(validator);
                ValidateE validateWithReportE = new ValidateE();
                validateWithReportE.setValidate(validateWithReport);
                ValidateResponseE responseE = stub.validate(validateWithReportE);

                this.validationResult = responseE.getValidateResponse().getValidationReport();
            } catch (AxisFault e) {
                log.error("a", e);
                log.error("An error occurred at validation time: " + e.getMessage());
            } catch (RemoteException e) {
                log.error("An error occurred at validation time: " + e.getMessage());
            }
        }
        return validationResult;
    }

    /**
     * Validate the document against the given validator
     *
     * @param document:  the document to decode (plain text or base64 encoded)
     * @param validator: the validator to use
     * @return the decoded document
     */
    public String decode(DataHandler document, String validator) throws UnkownValidatorExceptionException, InvalidObjectExceptionException {
        this.decodedMessage = null;

        if (document == null) {
            log.error("The document to validate is either null or empty");
        } else if (validator == null || validator.isEmpty()) {
            log.error("The validator to call is either null or empty");
        } else {
            try {
                getStub();
                Decode decode = new Decode();
                decode.setObject(document);
                decode.setValidator(validator);
                DecodeE decodeE = new DecodeE();
                decodeE.setDecode(decode);
                DecodeResponseE responseE = stub.decode(decodeE);

                this.decodedMessage = responseE.getDecodeResponse().getDecode();
            } catch (AxisFault e) {
                log.error("a", e);
                log.error("An error occurred at validation time: " + e.getMessage());
            } catch (RemoteException e) {
                log.error("An error occurred at validation time: " + e.getMessage());
            }
        }
        return decodedMessage;
    }


    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
