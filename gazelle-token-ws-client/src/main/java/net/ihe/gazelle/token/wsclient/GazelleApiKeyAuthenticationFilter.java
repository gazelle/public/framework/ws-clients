package net.ihe.gazelle.token.wsclient;

import com.fasterxml.jackson.databind.JsonNode;
import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import org.apache.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class GazelleApiKeyAuthenticationFilter implements Filter {

    public static final String GAZELLE_API_KEY_PREFIX = "GazelleApiKey";
    public static final String CONST_TOKEN_ASSERTION = "_const_token_assertion_";
    public static final String TOKEN_API_URL = "token_api_url";
    private PreferenceProvider preferenceProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
    }

    @Override
    public void doFilter(ServletRequest request, final ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(request instanceof HttpServletRequest)) {
            throw new ServletException("This filter can only process HttpServletRequest requests");
        } else {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final HttpServletResponse httpResponse = (HttpServletResponse) response;
            try {
                String authnHeader = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
                if (authnHeader != null && authnHeader.startsWith(GAZELLE_API_KEY_PREFIX)) {
                    String token = parseToken(authnHeader);
                    TokenServiceClient client = new TokenServiceClient(preferenceProvider.getString(TOKEN_API_URL));
                    JsonNode jsonToken = client.authenticate(token);
                    httpRequest.setAttribute(CONST_TOKEN_ASSERTION, jsonToken);
                } else {
                    httpRequest.setAttribute(CONST_TOKEN_ASSERTION, null);
                }
            } catch (ExpiredTokenException e) {
                httpResponse.sendError(Response.Status.UNAUTHORIZED.getStatusCode(), "Token expired");
            } catch (Exception e) {
                httpResponse.sendError(Response.Status.UNAUTHORIZED.getStatusCode());
            }
            chain.doFilter(httpRequest, httpResponse);
        }
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }

    private String parseToken(String token) {
        return token.replace(GAZELLE_API_KEY_PREFIX, "").trim();
    }
}