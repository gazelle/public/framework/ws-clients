package net.ihe.gazelle.token.wsclient;

import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorFactory;

public class GazelleApiKeyAuthenticatorFactory implements AuthenticatorFactory {

    @Override
    public Authenticator createAuthenticator() {
        return new GazelleApiKeyAuthenticator();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
