package net.ihe.gazelle.token.wsclient;

import com.fasterxml.jackson.databind.JsonNode;
import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.web.ServletContexts;
import org.jboss.seam.web.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class GazelleApiKeyAuthenticator implements Authenticator {


    @Override
    public AuthenticatorResource authenticate() {
        JsonNode jsonToken = getJsonToken();
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        if (jsonToken != null) {
            authenticatorResource = jsonNodeToIdentityToken();
            authenticatorResource.setLoggedIn(true);
            return authenticatorResource;
        }
        authenticatorResource.setLoggedIn(false);
        return authenticatorResource;
    }

    @Override
    public boolean isReadyToLogin() {
        JsonNode jsonToken = getJsonToken();
        return jsonToken != null;
    }

    @Override
    public String getLogoutType() {
        return "noLogout";
    }

    @Override
    public void logout() {
        Session.instance().invalidate();
    }

    private AuthenticatorResource jsonNodeToIdentityToken() {
        JsonNode jsonToken = getJsonToken();
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        authenticatorResource.setPrincipal(new GazelleApiKeyPrincipal(jsonToken));
        authenticatorResource.setUsername(jsonToken.findValue("username").textValue());
        authenticatorResource.setFirstName("GazelleApiKey");
        authenticatorResource.setLastName("Gazelle");
        authenticatorResource.setEmail("not provided");
        authenticatorResource.setOrganizationKeyword(jsonToken.findValue("organization").textValue());
        JsonNode roleNode = jsonToken.get("roles");
        List<String> roles = new ArrayList<>();
        if (roleNode.isArray()) {
            for (JsonNode role : roleNode) {
                roles.add(role.textValue());
            }
        }
        authenticatorResource.setRoles("["+StringUtils.join(roles, ",")+"]");
        return authenticatorResource;
    }

    private JsonNode getJsonToken() {
        HttpServletRequest httpServletRequest = getHttpServletRequest();
        if (httpServletRequest != null) {
            HttpSession session = httpServletRequest.getSession();
            if (session != null) {
                return (JsonNode) httpServletRequest.getAttribute(GazelleApiKeyAuthenticationFilter.CONST_TOKEN_ASSERTION);
            }
        }
        return null;
    }

    private HttpServletRequest getHttpServletRequest() {
        ServletContexts contexts = ServletContexts.getInstance();
        HttpServletRequest request = null;
        if (contexts != null)
            request = contexts.getRequest();
        return request;
    }
}
