package net.ihe.gazelle.token.wsclient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class TokenServiceClient {

    private static final Logger LOG = LoggerFactory.getLogger(TokenServiceClient.class);

    private final String tokenServiceUrl;

    public TokenServiceClient(String tokenServiceUrl) {
        this.tokenServiceUrl = tokenServiceUrl;
    }

    public JsonNode authenticate(String token) throws InvalidTokenException, ExpiredTokenException {
        if (tokenServiceUrl != null && token != null) {
            try {
                URL url = new URL(tokenServiceUrl + "/rest/v1/authn/user/" + token);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
                int status = connection.getResponseCode();

                if (status == Response.Status.BAD_REQUEST.getStatusCode() || status == Response.Status.NOT_FOUND.getStatusCode()) {
                    throw new InvalidTokenException("Bad request or Not found");
                } else if (status == Response.Status.GONE.getStatusCode()) {
                    throw new ExpiredTokenException("Token expired");
                } else if (status == Response.Status.OK.getStatusCode()) {
                    InputStream responseStream = connection.getInputStream();
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode node = mapper.readValue(responseStream, JsonNode.class);
                    connection.disconnect();
                    return node;
                } else {
                    throw new InvalidTokenException("Unknown error");
                }
            } catch (IOException e) {
                LOG.debug(e.getMessage(), e);
                throw new InvalidTokenException("Fail to convert to Json");
            }
        } else {
            LOG.debug("Both endpoint and token are required");
            throw new InvalidTokenException("Resquest is null");
        }
    }
}